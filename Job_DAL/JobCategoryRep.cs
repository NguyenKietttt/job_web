﻿using Job_Common.DAL;
using Job_Common.Rsp;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class JobCategoryRep : GenericRep<JobContext, JobCategory>
    {
        public static Dictionary<int, string> Get_ListJobCategories()
        {
            using var context = new JobContext();
            var res = context.JobCategory.AsNoTracking().ToDictionary(p => p.JobCategoryId, p => p.JobCategoryName);

            return res;
        }
    }
}
