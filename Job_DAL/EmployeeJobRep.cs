﻿using Job_Common.DAL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class EmployeeJobRep : GenericRep<JobContext, EmployeeJob>
    {
        #region Static Methods
        public static bool Check_EmployeeJob_JobID(int jobID)
        {
            using var context = new JobContext();
            bool check = false;

            var JobCount = (from e in context.Employee
                            join ej in context.EmployeeJob on e.EmployeeId equals ej.EmployeeId
                            where ej.JobId == jobID
                            select e).Count();

            if (JobCount > 0)
            {
                check = true;
            }

            return check;
        }
        #endregion

        public List<JobListEmployeeAppliedReq> Get_ListJob_Applied(int employeeID)
        {
            using var context = new JobContext();
            var listJob = from j in context.Job
                          join er in context.Employer on j.EmployerId equals er.EmployerId
                          join c in context.City on j.CityId equals c.CityId
                          join jt in context.JobType on j.JobTypeId equals jt.JobTypeId
                          join jc in context.JobCategory on j.JobCategoryId equals jc.JobCategoryId
                          join ej in context.EmployeeJob on j.JobId equals ej.JobId
                          join e in context.Employee on ej.EmployeeId equals e.EmployeeId
                          orderby ej.ApplyDate descending
                          where e.EmployeeId == employeeID
                          select new JobListEmployeeAppliedReq
                          {
                              JobID = j.JobId,
                              JobName = j.JobName,
                              PostDate = j.PostDate.Value.ToString("yyyy/MM/dd"),
                              RequireDate = j.RequireDate.Value.ToString("yyyy/MM/dd"),
                              Salary = j.Salary,
                              Experience = j.Experience,
                              City = c.CityName,
                              JobType = jt.JobTypeName,
                              JobCategory = jc.JobCategoryName,
                              CompanyName = er.CompanyName,
                              Image = er.Image
                          };

            return listJob.ToList();
        }

        public bool Check_EmployeeJob_ByEmployerID_JobID(int jobID, int employeeID)
        {
            using var context = new JobContext();
            bool check = false;

            var JobCount = (from e in context.Employee
                            join ej in context.EmployeeJob on e.EmployeeId equals ej.EmployeeId
                            where ej.JobId == jobID && e.EmployeeId == employeeID
                            select e).Count();

            if (JobCount > 0)
            {
                check = true;
            }

            return check;
        }

        public SingleRsp AddEmployeeJob(EmployeeJob employeeJob)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    context.Add(employeeJob);

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }
    }
}
