﻿using Job_Common.DAL;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class TitleRep : GenericRep<JobContext, Title>
    {
        public static Dictionary<int, string> Get_ListTitle()
        {
            using var context = new JobContext();
            var res = context.Title.AsNoTracking().ToDictionary(p => p.TitleId, p => p.TitleName);

            return res;
        }
    }
}
