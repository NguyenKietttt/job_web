﻿using Job_Common.DAL;
using Job_Common.Rsp;
using Job_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Job_Common.Req;

namespace Job_DAL
{
    public class EmployerRep : GenericRep<JobContext, Employer>
    {
        #region Static Methods
        public static int GetMax_EmployerID()
        {
            using var context = new JobContext();
            var res = context.Employer.AsNoTracking().OrderByDescending(p => p.EmployerId)
                .Select(p => p.EmployerId).FirstOrDefault();

            return res;
        }

        public static int Count_Employer()
        {
            using var context = new JobContext();
            var res = context.Employer.AsNoTracking().Count();

            return res;
        }

        public static EmployerReq Get_Employer_ByID(int EmployerID)
        {
            using var context = new JobContext();
            var employerDetails = from er in context.Employer
                                  join c in context.City on er.CityId equals c.CityId
                                  join a in context.Account on er.AccountId equals a.AccountId
                                  where er.EmployerId == EmployerID
                                  select new EmployerReq
                                  {
                                      AccountID = a.AccountId,
                                      EmployerID = er.EmployerId,
                                      CompanyName = er.CompanyName,
                                      Email = a.Email,
                                      Address = er.Address,
                                      Image = er.Image,
                                      CompanyDescription = er.CompanyDescription,
                                      CityName = c.CityName
                                  };

            return employerDetails.FirstOrDefault();
        }

        public static Employer Get_Employer_ByAccountID(string accountID)
        {
            using var context = new JobContext();
            Employer employer = context.Employer.Where(p => p.AccountId == accountID).FirstOrDefault();

            return employer;
        }
        #endregion

        public SingleRsp UpdateEmployerImg(Employer employer)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    context.Update(employer);

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }

        public SingleRsp Update_Employer(EmployerUpdateReq req)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var result = (from er in context.Employer
                                  where er.EmployerId == req.EmployerID
                                  select er).SingleOrDefault();

                    result.CompanyName = req.CompanyName;
                    result.Address = req.Address;
                    result.CompanyDescription = req.CompanyDescription;

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }
    }
}
