﻿using Job_Common.DAL;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class JobTypeRep : GenericRep<JobContext, JobType>
    {
        public static Dictionary<int, string> Get_ListJobType()
        {
            using var context = new JobContext();
            var res = context.JobType.AsNoTracking().ToDictionary(p => p.JobTypeId, p => p.JobTypeName);

            return res;
        }
    }
}
