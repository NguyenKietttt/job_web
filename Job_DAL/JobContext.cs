﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Job_DAL.Models
{
    public partial class JobContext : DbContext
    {
        public JobContext()
        {
        }

        public JobContext(DbContextOptions<JobContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EmployeeJob> EmployeeJob { get; set; }
        public virtual DbSet<Employer> Employer { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<JobCategory> JobCategory { get; set; }
        public virtual DbSet<JobType> JobType { get; set; }
        public virtual DbSet<Title> Title { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=tcp:jobweb.database.windows.net,1433;Initial Catalog=Job;Persist Security Info=False;User ID=tk45456;Password=Concaheo45456;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasMaxLength(20);

                entity.Property(e => e.Email).HasMaxLength(40);

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsFixedLength();

                entity.Property(e => e.UserTypeId).HasColumnName("UserTypeID");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.UserTypeId)
                    .HasConstraintName("FK__Account__UserTyp__2BFE89A6");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.Property(e => e.CityId)
                    .HasColumnName("CityID")
                    .HasMaxLength(5);

                entity.Property(e => e.CityName).HasMaxLength(100);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasMaxLength(20);

                entity.Property(e => e.CoverLetter).HasColumnType("ntext");

                entity.Property(e => e.Cv)
                    .HasColumnName("CV")
                    .HasColumnType("ntext");

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK__Employee__Accoun__2CF2ADDF");
            });

            modelBuilder.Entity<EmployeeJob>(entity =>
            {
                entity.HasKey(e => new { e.EmployeeId, e.JobId });

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.ApplyDate).HasColumnType("date");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.EmployeeJob)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__EmployeeJ__Emplo__282DF8C2");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.EmployeeJob)
                    .HasForeignKey(d => d.JobId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__EmployeeJ__JobID__29221CFB");
            });

            modelBuilder.Entity<Employer>(entity =>
            {
                entity.Property(e => e.EmployerId)
                    .HasColumnName("EmployerID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasMaxLength(20);

                entity.Property(e => e.Address).HasMaxLength(256);

                entity.Property(e => e.CityId)
                    .IsRequired()
                    .HasColumnName("CityID")
                    .HasMaxLength(5);

                entity.Property(e => e.CompanyDescription).HasColumnType("ntext");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Image).HasColumnType("ntext");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Employer)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK__Employer__Accoun__787EE5A0");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Employer)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employer__CityID__778AC167");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.Property(e => e.JobId)
                    .HasColumnName("JobID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CityId)
                    .HasColumnName("CityID")
                    .HasMaxLength(5);

                entity.Property(e => e.EmployerId).HasColumnName("EmployerID");

                entity.Property(e => e.Experience).HasMaxLength(50);

                entity.Property(e => e.JobCategoryId).HasColumnName("JobCategoryID");

                entity.Property(e => e.JobDescription).HasColumnType("ntext");

                entity.Property(e => e.JobName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.JobRequire).HasColumnType("ntext");

                entity.Property(e => e.JobTypeId).HasColumnName("JobTypeID");

                entity.Property(e => e.PostDate).HasColumnType("date");

                entity.Property(e => e.RequireDate).HasColumnType("date");

                entity.Property(e => e.Salary).HasMaxLength(100);

                entity.Property(e => e.TitleId).HasColumnName("TitleID");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Job)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK__Job__CityID__7B5B524B");

                entity.HasOne(d => d.Employer)
                    .WithMany(p => p.Job)
                    .HasForeignKey(d => d.EmployerId)
                    .HasConstraintName("FK__Job__EmployerID__7C4F7684");

                entity.HasOne(d => d.JobCategory)
                    .WithMany(p => p.Job)
                    .HasForeignKey(d => d.JobCategoryId)
                    .HasConstraintName("FK__Job__JobCategory__797309D9");

                entity.HasOne(d => d.JobType)
                    .WithMany(p => p.Job)
                    .HasForeignKey(d => d.JobTypeId)
                    .HasConstraintName("FK__Job__JobTypeID__7A672E12");

                entity.HasOne(d => d.Title)
                    .WithMany(p => p.Job)
                    .HasForeignKey(d => d.TitleId)
                    .HasConstraintName("FK__Job__TitleID__7D439ABD");
            });

            modelBuilder.Entity<JobCategory>(entity =>
            {
                entity.Property(e => e.JobCategoryId)
                    .HasColumnName("JobCategoryID")
                    .ValueGeneratedNever();

                entity.Property(e => e.JobCategoryName).HasMaxLength(100);
            });

            modelBuilder.Entity<JobType>(entity =>
            {
                entity.Property(e => e.JobTypeId)
                    .HasColumnName("JobTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.JobTypeName).HasMaxLength(20);
            });

            modelBuilder.Entity<Title>(entity =>
            {
                entity.Property(e => e.TitleId)
                    .HasColumnName("TitleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.TitleName).HasMaxLength(20);
            });

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.Property(e => e.UserTypeId)
                    .HasColumnName("UserTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.UserTypeName).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
