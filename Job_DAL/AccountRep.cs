﻿using Job_Common.DAL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class AccountRep : GenericRep<JobContext, Account>
    {
        #region Static Methods
        public static SingleRsp Update_Email(EmailReq emailReq)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var result = (from a in context.Account
                                  where a.AccountId == emailReq.AccountID
                                  select a).SingleOrDefault();

                    result.Email = emailReq.Email;

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }
        #endregion

        public List<string> Get_ListAccountID()
        {
            using var context = new JobContext();
            var res = context.Account.AsNoTracking().Select(p => p.AccountId).ToList();

            return res;
        }

        public int? Get_UserType_Account(string AccountID)
        {
            using var context = new JobContext();
            int? res = context.Account.AsNoTracking().Where(p => p.AccountId == AccountID).FirstOrDefault().UserTypeId;

            return res;
        }

        public List<string> Authenticate(AccountLoginReq account)
        {
            using var context = new JobContext();
            var listAccounts = context.Account.FromSqlRaw("exec uspLogin {0}, {1}",
                account.AccountID, account.Password).Select(p => p.AccountId).ToList();

            return listAccounts;
        }

        public SingleRsp CreateAccountEmployer(CreateAccountEmployerHaveIDReq2 account)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var executeQuery = context.Database.ExecuteSqlRaw("exec uspAddAccount {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                        account.AccountID, account.Password, account.Email, account.EmployerId,
                        account.CompanyID, account.CompanyDescription, account.Address, account.CityName);

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }

        public SingleRsp CreateAccountEmployee(CreateAccountEmployeeHaveIDReq account)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var executeQuery = context.Database.ExecuteSqlRaw("exec uspAddAccountEmployee {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                        account.AccountID, account.Password, account.Email, account.EmployeeId,
                        account.EmployeeName, account.Phone, account.CoverLetter, null);

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }
    }
}
