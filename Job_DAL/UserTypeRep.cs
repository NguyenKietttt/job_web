﻿using Job_Common.DAL;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class UserTypeRep : GenericRep<JobContext, UserType>
    {
        public static Dictionary<int, string> Get_ListUserType()
        {
            using var context = new JobContext();
            var res = context.UserType.AsNoTracking().ToDictionary(p => p.UserTypeId, p => p.UserTypeName);

            return res;
        }
    }
}
