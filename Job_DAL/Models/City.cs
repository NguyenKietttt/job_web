﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class City
    {
        public City()
        {
            Employer = new HashSet<Employer>();
            Job = new HashSet<Job>();
        }

        public string CityId { get; set; }
        public string CityName { get; set; }

        public virtual ICollection<Employer> Employer { get; set; }
        public virtual ICollection<Job> Job { get; set; }
    }
}
