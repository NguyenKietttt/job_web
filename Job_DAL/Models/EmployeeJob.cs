﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class EmployeeJob
    {
        public int EmployeeId { get; set; }
        public int JobId { get; set; }
        public DateTime? ApplyDate { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Job Job { get; set; }
    }
}
