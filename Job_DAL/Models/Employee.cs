﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class Employee
    {
        public Employee()
        {
            EmployeeJob = new HashSet<EmployeeJob>();
        }

        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Phone { get; set; }
        public string CoverLetter { get; set; }
        public string Cv { get; set; }
        public string AccountId { get; set; }

        public virtual Account Account { get; set; }
        public virtual ICollection<EmployeeJob> EmployeeJob { get; set; }
    }
}
