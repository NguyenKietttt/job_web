﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class Title
    {
        public Title()
        {
            Job = new HashSet<Job>();
        }

        public int TitleId { get; set; }
        public string TitleName { get; set; }

        public virtual ICollection<Job> Job { get; set; }
    }
}
