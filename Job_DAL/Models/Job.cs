﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class Job
    {
        public Job()
        {
            EmployeeJob = new HashSet<EmployeeJob>();
        }

        public int JobId { get; set; }
        public string JobName { get; set; }
        public string JobDescription { get; set; }
        public string JobRequire { get; set; }
        public string Salary { get; set; }
        public DateTime? PostDate { get; set; }
        public DateTime? RequireDate { get; set; }
        public string Experience { get; set; }
        public int? EmployerId { get; set; }
        public int? TitleId { get; set; }
        public int? JobCategoryId { get; set; }
        public int? JobTypeId { get; set; }
        public string CityId { get; set; }

        public virtual City City { get; set; }
        public virtual Employer Employer { get; set; }
        public virtual JobCategory JobCategory { get; set; }
        public virtual JobType JobType { get; set; }
        public virtual Title Title { get; set; }
        public virtual ICollection<EmployeeJob> EmployeeJob { get; set; }
    }
}
