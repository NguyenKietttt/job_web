﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class Account
    {
        public Account()
        {
            Employee = new HashSet<Employee>();
            Employer = new HashSet<Employer>();
        }

        public string AccountId { get; set; }
        public byte[] PasswordHash { get; set; }
        public string Email { get; set; }
        public Guid? Salt { get; set; }
        public int? UserTypeId { get; set; }

        public virtual UserType UserType { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Employer> Employer { get; set; }
    }
}
