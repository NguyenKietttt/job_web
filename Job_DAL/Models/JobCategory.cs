﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class JobCategory
    {
        public JobCategory()
        {
            Job = new HashSet<Job>();
        }

        public int JobCategoryId { get; set; }
        public string JobCategoryName { get; set; }

        public virtual ICollection<Job> Job { get; set; }
    }
}
