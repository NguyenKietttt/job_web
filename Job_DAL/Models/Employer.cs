﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class Employer
    {
        public Employer()
        {
            Job = new HashSet<Job>();
        }

        public int EmployerId { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string AccountId { get; set; }
        public string CityId { get; set; }
        public string Image { get; set; }
        public string CompanyDescription { get; set; }

        public virtual Account Account { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<Job> Job { get; set; }
    }
}
