﻿using System;
using System.Collections.Generic;

namespace Job_DAL.Models
{
    public partial class UserType
    {
        public UserType()
        {
            Account = new HashSet<Account>();
        }

        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
