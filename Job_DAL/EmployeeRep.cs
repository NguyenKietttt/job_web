﻿using Job_Common.DAL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class EmployeeRep : GenericRep<JobContext, Employee>
    {
        #region Static Methods
        public static int GetMax_EmployeeID()
        {
            using var context = new JobContext();
            var res = context.Employee.AsNoTracking().OrderByDescending(p => p.EmployeeId).Select(p => p.EmployeeId).FirstOrDefault();

            return res;
        }

        public static int Count_Employee()
        {
            using var context = new JobContext();
            var res = context.Employee.AsNoTracking().Count();

            return res;
        }

        public static EmployeeReq Get_Info_Employee(int EmployeeID)
        {
            using var context = new JobContext();
            var employeeDetails = from e in context.Employee
                                  join a in context.Account on e.AccountId equals a.AccountId
                                  where e.EmployeeId == EmployeeID
                                  select new EmployeeReq
                                  {
                                      AccountID = a.AccountId,
                                      EmployeeID = e.EmployeeId,
                                      EmployeeName = e.EmployeeName,
                                      Email = a.Email,
                                      Phone = e.Phone,
                                      CoverLetter = e.CoverLetter
                                  };

            return employeeDetails.FirstOrDefault();
        }

        public static Employee Get_Employee_ByAccountID(string accountID)
        {
            using var context = new JobContext();
            Employee employee = context.Employee.Where(p => p.AccountId == accountID).FirstOrDefault();

            return employee;
        }

        public static Employee Get_Employee_ByID(int Id)
        {
            using var context = new JobContext();
            Employee employee = context.Employee.Where(p => p.EmployeeId == Id).FirstOrDefault();

            return employee;
        }

        public static SingleRsp Update_EmployeeCV(int employeeId, string cv)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var result = (from e in context.Employee
                                  where e.EmployeeId == employeeId
                                  select e).SingleOrDefault();

                    result.Cv = cv;

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }
        #endregion

        public static Account Get_Account_ByEmployeeID(int employeeID)
        {
            using var context = new JobContext();
            var account = (from e in context.Employee
                          join a in context.Account on e.AccountId equals a.AccountId
                          where e.EmployeeId == employeeID
                          select a).FirstOrDefault();

            return account;
        }

        public ImgReq Get_Image_ByAccountID(string AccountID)
        {
            using var context = new JobContext();
            var employerDetails = from er in context.Employer
                                  join a in context.Account on er.AccountId equals a.AccountId
                                  where a.AccountId == AccountID
                                  select new ImgReq
                                  {
                                      AccountID = a.AccountId,
                                      Image = er.Image,
                                  };

            return employerDetails.FirstOrDefault();
        }

        public List<EmployeeRes> Get_ListEmployee_ByJobID(int JobID)
        {
            using var context = new JobContext();
            var jobDetail = from j in context.Job
                            join ej in context.EmployeeJob on j.JobId equals ej.JobId
                            join e in context.Employee on ej.EmployeeId equals e.EmployeeId
                            join a in context.Account on e.AccountId equals a.AccountId
                            where j.JobId == JobID
                            orderby ej.ApplyDate descending
                            select new EmployeeRes
                            {
                                EmployeeName = e.EmployeeName,
                                Phone = e.Phone,
                                Email = a.Email,
                                CoverLetter = e.CoverLetter,
                                CV = e.Cv,
                                ApplyDate = ej.ApplyDate.Value.ToString("yyyy-MM-dd")
                            };

            return jobDetail.ToList();
        }

        public SingleRsp Update_Employee(EmployeeUpdateNonIDReq req)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var result = (from e in context.Employee
                                  where e.EmployeeId == req.EmployeeID
                                  select e).SingleOrDefault();

                    result.EmployeeName = req.EmployeeName;
                    result.Phone = req.Phone;
                    result.CoverLetter = req.CoverLetter;

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }
    }
}
