﻿using Job_Common.DAL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class JobRep : GenericRep<JobContext, Job>
    {
        #region Static Methods
        public static int Get_Job_ByID(int jobID)
        {
            using var context = new JobContext();
            var res = context.Job.Where(p => p.JobId == jobID).Count();

            return res;
        }

        public static JobDetailReq Get_JobDetail(int JobID)
        {
            using JobContext context = new JobContext();
            var jobDetail = from j in context.Job
                            join er in context.Employer on j.EmployerId equals er.EmployerId
                            join c in context.City on j.CityId equals c.CityId
                            join t in context.Title on j.TitleId equals t.TitleId
                            join jc in context.JobCategory on j.JobCategoryId equals jc.JobCategoryId
                            join jt in context.JobType on j.JobTypeId equals jt.JobTypeId
                            join a in context.Account on er.AccountId equals a.AccountId
                            where j.JobId == JobID
                            select new JobDetailReq
                            {
                                JobName = j.JobName,
                                JobDescription = j.JobDescription,
                                JobRequire = j.JobRequire,
                                Salary = j.Salary,
                                PostDate = j.PostDate,
                                RequireDate = j.RequireDate,
                                Experience = j.Experience,
                                EmployerId = er.EmployerId,
                                CompanyName = er.CompanyName,
                                Address = er.Address,
                                Email = a.Email,
                                Title = t.TitleName,
                                JobType = jt.JobTypeName,
                                City = c.CityName,
                                JobCategory = jc.JobCategoryName,
                                Image = er.Image
                            };

            return jobDetail.FirstOrDefault();
        }
        #endregion

        public int GetMax_JobID()
        {
            using var context = new JobContext();
            var res = context.Job.AsNoTracking().OrderByDescending(p => p.JobId).Select(p => p.JobId).FirstOrDefault();

            return res;
        }

        public int Count_Job()
        {
            using JobContext context = new JobContext();
            var res = context.Job.AsNoTracking().Count();

            return res;
        }

        public List<JobListReq> Get_ListJob_ByKeyWord(
            string keyWord = null, string city = null, string jobType = null)
        {
            using var context = new JobContext();
            var listJobByNameReq = from j in context.Job
                                   join er in context.Employer on j.EmployerId equals er.EmployerId
                                   join c in context.City on j.CityId equals c.CityId
                                   join jt in context.JobType on j.JobTypeId equals jt.JobTypeId
                                   join a in context.Account on er.AccountId equals a.AccountId
                                   where j.JobName.Contains(keyWord == null ? " " : keyWord) &&
                                         (c.CityName == city || string.IsNullOrEmpty(city)) &&
                                         (jt.JobTypeName == jobType || string.IsNullOrEmpty(jobType))
                                   select new JobListReq
                                   {
                                       JobID = j.JobId,
                                       EmployerID = er.EmployerId,
                                       CompanyName = er.CompanyName,
                                       JobName = j.JobName,
                                       PostDate = j.PostDate.Value.ToString("yyyy/MM/dd"),
                                       RequireDate = j.RequireDate.Value.ToString("yyyy/MM/dd"),
                                       Salary = j.Salary,
                                       Experience = j.Experience,
                                       City = c.CityName,
                                       Email = a.Email,
                                       Address = er.Address,
                                       CompanyDescription = er.CompanyDescription,
                                       Image = er.Image,
                                       JobType = jt.JobTypeName
                                   };

            return listJobByNameReq.ToList();
        }

        public List<JobListReq> Get_ListJob_ByCompanyName(
            string companyName = null, string city = null, string jobType = null)
        {
            using var context = new JobContext();
            var listJobByNameReq = from j in context.Job
                                   join er in context.Employer on j.EmployerId equals er.EmployerId
                                   join c in context.City on j.CityId equals c.CityId
                                   join jt in context.JobType on j.JobTypeId equals jt.JobTypeId
                                   join a in context.Account on er.AccountId equals a.AccountId
                                   where er.CompanyName.Contains(companyName == null ? " " : companyName) &&
                                         (c.CityName == city || string.IsNullOrEmpty(city)) &&
                                         (jt.JobTypeName == jobType || string.IsNullOrEmpty(jobType))
                                   select new JobListReq
                                   {
                                       JobID = j.JobId,
                                       EmployerID = er.EmployerId,
                                       CompanyName = er.CompanyName,
                                       JobName = j.JobName,
                                       PostDate = j.PostDate.Value.ToString("yyyy/MM/dd"),
                                       RequireDate = j.RequireDate.Value.ToString("yyyy/MM/dd"),
                                       Salary = j.Salary,
                                       Experience = j.Experience,
                                       City = c.CityName,
                                       Email = a.Email,
                                       Address = er.Address,
                                       CompanyDescription = er.CompanyDescription,
                                       Image = er.Image,
                                       JobType = jt.JobTypeName
                                   };

            return listJobByNameReq.ToList();
        }

        public List<JobListEmployerReq> Get_ListJob_ByEmployerID(int EmployerID)
        {
            using var context = new JobContext();
            var listJob = from j in context.Job
                          join er in context.Employer on j.EmployerId equals er.EmployerId
                          join c in context.City on j.CityId equals c.CityId
                          join a in context.Account on er.AccountId equals a.AccountId
                          join jt in context.JobType on j.JobTypeId equals jt.JobTypeId
                          join jc in context.JobCategory on j.JobCategoryId equals jc.JobCategoryId
                          orderby j.PostDate descending
                          where er.EmployerId == EmployerID
                          select new JobListEmployerReq
                          {
                              JobID = j.JobId,
                              JobName = j.JobName,
                              PostDate = j.PostDate.Value.ToString("yyyy/MM/dd"),
                              RequireDate = j.RequireDate.Value.ToString("yyyy/MM/dd"),
                              Salary = j.Salary,
                              Experience = j.Experience,
                              City = c.CityName,
                              JobType = jt.JobTypeName,
                              JobCategory = jc.JobCategoryName
                          };

            return listJob.ToList();
        }

        public List<JobListReq> Get_All_ListJob()
        {
            using JobContext context = new JobContext();
            var listJob = from j in context.Job
                          join er in context.Employer on j.EmployerId equals er.EmployerId
                          join c in context.City on j.CityId equals c.CityId
                          join a in context.Account on er.AccountId equals a.AccountId
                          join jt in context.JobType on j.JobTypeId equals jt.JobTypeId
                          orderby j.PostDate descending
                          select new JobListReq
                          {
                              JobID = j.JobId,
                              EmployerID = er.EmployerId,
                              CompanyName = er.CompanyName,
                              JobName = j.JobName,
                              PostDate = j.PostDate.Value.ToString("yyyy/MM/dd"),
                              RequireDate = j.RequireDate.Value.ToString("yyyy/MM/dd"),
                              Salary = j.Salary,
                              Experience = j.Experience,
                              City = c.CityName,
                              Email = a.Email,
                              Address = er.Address,
                              CompanyDescription = er.CompanyDescription,
                              Image = er.Image,
                              JobType = jt.JobTypeName
                          };

            return listJob.ToList();
        }

        public SingleRsp AddJobs(List<Job> listJobs)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    context.AddRange(listJobs);

                    context.SaveChanges();
                    tran.Commit();

                    res.SetMessage("Success!!!");
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }

        public SingleRsp Update_RequireDate(int jobID, DateTime requireDate)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var result = (from p in context.Job
                                  where p.JobId == jobID
                                  select p).SingleOrDefault();

                    result.PostDate = DateTime.Now.Date;
                    result.RequireDate = requireDate;

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }

        public SingleRsp Delete_Job(int jobID)
        {
            var res = new SingleRsp();

            using (var context = new JobContext())
            {
                using var tran = context.Database.BeginTransaction();
                try
                {
                    var Job = (from j in context.Job
                               where j.JobId == jobID
                               select j).FirstOrDefault();

                    context.Job.Remove(Job);

                    context.SaveChanges();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    res.SetError(ex.StackTrace);
                }
            }

            return res;
        }
    }
}
