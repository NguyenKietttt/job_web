﻿using Job_Common.DAL;
using Job_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Job_DAL
{
    public class CityRep : GenericRep<JobContext, City>
    {
        public static Dictionary<string, string> Get_ListCities()
        {
            using var context = new JobContext();
            var res = context.City.AsNoTracking().ToDictionary(p => p.CityId, p => p.CityName);

            return res;
        }
    }
}
