# Welcome to Job_Web!

The website helps people looking for jobs and applicants.

## Prerequisite
#### For the server-side
- ASP .NET core >= 3.1
- SQL Server >= 2017

#### For the cline-side
- NPM >= 6.14.8
- NODE >= v12.19.0
- REACT >= 16.13.1


## Install
**node**
Follow this [link](https://nodejs.org/en/download/)
> npm install

**axios**
> npm install axios

**Sweetalert**
> npm install sweetalert

## Data Sources:
* Aggregated data sources:
  * Jobs Source: [https://careerbuilder.vn](https://careerbuilder.vn)


## Features

Rest API

Apply Job

Post Job

## Author
* Trương Vĩnh Thoại (Front-End)
* Nguyễn Đỗ Tuấn Kiệt (Back-End)

