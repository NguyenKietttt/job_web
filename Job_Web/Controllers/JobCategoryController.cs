﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Job_BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobCategoryController : ControllerBase
    {
        private readonly JobCategorySvc _svc;

        public JobCategoryController()
        {
            _svc = new JobCategorySvc();
        }

        [HttpGet("Get-list-Category")]
        public IActionResult Get_CityNames_FromDB()
        {
            var res = _svc.Get_JobCategory_FromDB();

            return Ok(res);
        }
    }
}
