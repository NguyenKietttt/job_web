﻿using System.Collections.Generic;
using System.IO;
using Job_BLL;
using Job_Common.Req;
using Job_Common.Rsp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeSvc _svc;

        public EmployeeController()
        {
            _svc = new EmployeeSvc();
        }

        [HttpGet("Get-image-by-AccountID")]
        public IActionResult Get_Image_ByAccountID(string AccountID)
        {
            var res = _svc.Get_Image_ByAccountID(AccountID);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }

        [Authorize]
        [HttpGet("Get-list-Employee-by-JobID")]
        public IActionResult Get_ListEmployee_ByJobID(int jobID)
        {
            var res = _svc.Get_ListEmployee_ByJobID(jobID);

            return Ok(res);
        }

        [HttpGet("Get-employee-by-id")]
        public IActionResult Get_Info_Employee(int EmployeeID)
        {
            var res = _svc.Get_Info_Employee(EmployeeID);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }

        [Authorize]
        [HttpPost("Update-employee")]
        public IActionResult Update_Employee([FromBody] EmployeeUpdateHaveIDReq req)
        {
            var res = _svc.Update_Employee(req);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok("Update Successful!!!");
        }

        //[EnableCors("default")]
        //[HttpPost("Apply-job")]
        //public IActionResult Create([FromForm] EmployeeReq employee)
        //{
        //    var res = _svc.Add_EmployeeAsync(employee);

        //    if (!res.Result.Success)
        //    {
        //        return BadRequest(res);
        //    }

        //    return Ok("Apply Successful!!!");
        //}
    }
}
