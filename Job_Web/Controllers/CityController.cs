﻿using Job_BLL;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly CitySvc _svc;
        public CityController()
        {
            _svc = new CitySvc();
        }

        [HttpGet("Get-list-city")]
        public IActionResult Get_CityNames_FromDB()
        {
            var res = _svc.Get_CityNames_FromDB();

            return Ok(res);
        }
    }
}
