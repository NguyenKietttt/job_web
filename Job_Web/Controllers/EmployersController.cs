﻿using Job_BLL;
using Job_Common.Req;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployersController : ControllerBase
    {
        private readonly EmployerSvc _svc;

        public EmployersController()
        {
            _svc = new EmployerSvc();
        }

        [HttpGet("Get-employer-by-id")]
        public IActionResult Get_Employer_ByID(int EmployerID)
        {
            var res = _svc.Get_Employer_ByID(EmployerID);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }


        [HttpPost("Upload-Image")]
        public IActionResult UploadLogo([FromForm] ImageReq imageReq)
        {
            var res = _svc.Upload_EmployerImgAsync(imageReq);

            if (!res.Result.Success)
            {
                return BadRequest(res);
            }

            return Ok(res.Result);
        }

        [Authorize]
        [HttpPost("Update-employer")]
        public IActionResult Update_Employer([FromBody] EmployerUpdateReq req)
        {
            var res = _svc.Update_Employer(req);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }
    }
}
