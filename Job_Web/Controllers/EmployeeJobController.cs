﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Job_BLL;
using Job_Common.Req;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeJobController : ControllerBase
    {
        private readonly EmployeeJobSvc _svc;

        public EmployeeJobController()
        {
            _svc = new EmployeeJobSvc();
        }

        [EnableCors("CorsApi")]
        [Authorize]
        [HttpPost("Apply-job")]
        public IActionResult Apply_Job([FromForm] ApplyJobReq req)
        {
            var res = _svc.ApplyJobAsync(req);

            if (!res.Result.Success)
            {
                return BadRequest(res);
            }

            return Ok("Apply Successful!!!");
        }

        [Authorize]
        [HttpGet("Get-listjob-employee-applied")]
        public IActionResult Get_ListJob_Applied(int employeeID, int page)
        {
            var res = _svc.Get_ListJob_Applied(employeeID, page);

            return Ok(res);
        }
    }
}
