﻿using Job_BLL;
using Job_Common.Req;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly JobSvc _svc;

        public JobController()
        {
            _svc = new JobSvc();
        }

        [HttpGet("Get-list-Salary-for-search-job")]
        public IActionResult Get_ListSalary()
        {
            var res = _svc.Get_ListSalary_For_SearchJob();

            return Ok(res);
        }

        [HttpGet("Get-list-Salary-for-add-job")]
        public IActionResult Get_ListSalary_For_Addjob()
        {
            var res = _svc.Get_ListSalary_For_AddJob();

            return Ok(res);
        }

        [HttpGet("Get-job-detail")]
        public IActionResult Get_JobDetail(int jobID)
        {
            var res = _svc.Get_JobDetail(jobID);

            return Ok(res);
        }

        [HttpGet("Get-listjob-by-employerID")]
        public IActionResult Get_ListJob_ByEmployerID(int employerID, int page)
        {
            var res = _svc.Get_ListJob_ByEmployerID(employerID, page);

            return Ok(res);
        }

        [HttpGet("Get-all_jobs")]
        public IActionResult Get_All_ListJobs(int page)
        {
            var res = _svc.Get_All_ListJob(page);

            return Ok(res);
        }

        [HttpPost("Filter-list-job")]
        public IActionResult Get_ListJob_Page([FromBody] JobFilterReq req)
        {
            var res = _svc.Get_ListJob_Page(req);

            return Ok(res);
        }

        [Authorize]
        [HttpPost("Add-jobs")]
        public IActionResult GetCasePages([FromBody] JobAddReq jobAdd)
        {
            var res = _svc.Add_Job(jobAdd);

            if (!res.Success)
            {
                res.SetError("Please check data again!!!");

                return BadRequest(res);
            }

            return Ok(res);
        }

        [Authorize]
        [HttpPost("Update-job-require-date")]
        public IActionResult Update_RequireDate(UpdateRequireDateReq updateReq)
        {
            var res = _svc.Update_RequireDate(updateReq.JobID, updateReq.RequireDate);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }

        [Authorize]
        [HttpDelete("Delete-job")]
        public IActionResult Delete_Job(int jobID)
        {
            var res = _svc.Delete_Job(jobID);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }
    }
}
