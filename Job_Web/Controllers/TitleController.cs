﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Job_BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TitleController : ControllerBase
    {
        private readonly TitleSvc _svc;

        public TitleController()
        {
            _svc = new TitleSvc();
        }

        [HttpGet("Get-list-Title")]
        public IActionResult Get_ListTile()
        {
            var res = _svc.Get_ListTitle();

            return Ok(res);
        }
    }
}
