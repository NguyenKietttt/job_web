﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Job_BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserTypeController : ControllerBase
    {
        private readonly UserTypeSvc _svc;

        public UserTypeController()
        {
            _svc = new UserTypeSvc();
        }

        [HttpGet("Get-list-UserType")]
        public IActionResult Get_ListUserType()
        {
            var res = _svc.Get_ListUserType();

            return Ok(res);
        }
    }
}
