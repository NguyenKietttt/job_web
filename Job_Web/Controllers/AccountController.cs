﻿using Job_BLL;
using Job_Common.Req;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountSvc _svc;

        public AccountController()
        {
            _svc = new AccountSvc();
        }

        [HttpPost("Check")]
        public IActionResult Check([FromBody] AccountLoginReq req)
        {
            // Username and password < 20 char
            var res = _svc.Authenticate(req);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }

        [HttpPost("Create-Account-Employer")]
        public IActionResult CreateAccountEmployer([FromBody] CreateAccountEmployerNonIDReq req)
        {
            // Username and password < 20 char
            var res = _svc.CreateAccountEmployer(req);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok(res);
        }

        [HttpPost("Create-Account-Employee")]
        public IActionResult CreateAccountEmployee([FromBody] CreateAccountEmployeeReq req)
        {
            // Username and password < 20 char
            var res = _svc.CreateAccountEmployee(req);

            if (!res.Success)
            {
                return BadRequest(res);
            }

            return Ok("Create Successful!!!");
        }
    }
}
