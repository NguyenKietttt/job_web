﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Job_BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Job_Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobTypeController : ControllerBase
    {
        private readonly JobTypeSvc _svc;

        public JobTypeController()
        {
            _svc = new JobTypeSvc();
        }

        [HttpGet("Get-list-jobtype")]
        public IActionResult Get_ListTile()
        {
            var res = _svc.Get_ListJobType();

            return Ok(res);
        }
    }
}
