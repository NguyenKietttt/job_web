﻿namespace Job_Common.Req
{
    public class EmailReq
    {
        public string AccountID { get; set; }
        public string Email { get; set; }
    }
}
