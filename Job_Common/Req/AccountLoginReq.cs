﻿namespace Job_Common.Req
{
    public class AccountLoginReq
    {
        public string AccountID { get; set; }
        public string Password { get; set; }
    }
}
