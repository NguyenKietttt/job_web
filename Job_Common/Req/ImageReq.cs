﻿using Microsoft.AspNetCore.Http;

namespace Job_Common.Req
{
    public class ImageReq
    {
        public IFormFile File { get; set; }
        public string AccountID { get; set; }
    }
}
