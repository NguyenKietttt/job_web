﻿using System;

namespace Job_Common.Req
{
    public class JobDetailReq
    {
        public string JobName { get; set; }
        public string JobDescription { get; set; }
        public string JobRequire { get; set; }
        public string Salary { get; set; }
        public DateTime? PostDate { get; set; }
        public DateTime? RequireDate { get; set; }
        public string Experience { get; set; }
        public int? EmployerId { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string JobType { get; set; }
        public string City { get; set; }
        public string JobCategory { get; set; }
        public string Image { get; set; }
    }
}
