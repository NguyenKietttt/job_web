﻿using System;

namespace Job_Common.Req
{
    public class JobListReq
    {
        public int JobID { get; set; }
        public int EmployerID { get; set; }
        public string CompanyName { get; set; }
        public string JobName { get; set; }
        public string PostDate { get; set; }
        public string RequireDate { get; set; }
        public string Salary { get; set; }
        public string Experience { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CompanyDescription { get; set; }
        public string Image { get; set; }
        public string JobType { get; set; }
    }
}
