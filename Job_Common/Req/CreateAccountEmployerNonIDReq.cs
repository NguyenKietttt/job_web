﻿namespace Job_Common.Req
{
    public class CreateAccountEmployerNonIDReq
    {
        public string AccountID { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string CompanyDescription { get; set; }
    }
}
