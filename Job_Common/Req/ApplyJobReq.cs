﻿using Microsoft.AspNetCore.Http;

namespace Job_Common.Req
{
    public class ApplyJobReq
    {
        public int EmployeeId { get; set; }
        public int JobId { get; set; }
        public IFormFile CV { get; set; }
    }
}
