﻿namespace Job_DAL
{
    public class CreateAccountEmployerHaveIDReq2
    {
        public string AccountID { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int EmployerId { get; set; }
        public string CompanyID { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string CompanyDescription { get; set; }
    }
}
