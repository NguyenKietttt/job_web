﻿using System.Collections.Generic;

namespace Job_Common.Req
{
    public class EmployerJobReq
    {
        public EmployerReq Employer { get; set; }
        public List<JobListEmployerReq> ListJobs { get; set; }
    }
}
