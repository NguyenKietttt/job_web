﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Job_Common.Req
{
    public class JobListEmployeeAppliedReq
    {
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string PostDate { get; set; }
        public string RequireDate { get; set; }
        public string Salary { get; set; }
        public string Experience { get; set; }
        public string JobType { get; set; }
        public string City { get; set; }
        public string JobCategory { get; set; }
        public string CompanyName { get; set; }
        public string Image { get; set; }
    }
}
