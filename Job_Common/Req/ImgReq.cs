﻿namespace Job_Common.Req
{
    public class ImgReq
    {
        public string AccountID { get; set; }
        public string Image { get; set; }
    }
}
