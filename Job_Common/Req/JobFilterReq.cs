﻿namespace Job_Common.Req
{
    public class JobFilterReq
    {
        public string KeyWord { get; set; }
        public string City { get; set; }
        public string JobType { get; set; }
        public string Salary { get; set; }
        public int Page { get; set; }
    }
}
