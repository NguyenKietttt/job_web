﻿namespace Job_Common.Req
{
    public class EmployeeUpdateNonIDReq
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Phone { get; set; }
        public string CoverLetter { get; set; }
    }
}
