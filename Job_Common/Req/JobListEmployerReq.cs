﻿namespace Job_Common.Req
{
    public class JobListEmployerReq
    {
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string PostDate { get; set; }
        public string RequireDate { get; set; }
        public string Salary { get; set; }
        public string Experience { get; set; }
        public string JobType { get; set; }
        public string City { get; set; }
        public string JobCategory { get; set; }
    }
}
