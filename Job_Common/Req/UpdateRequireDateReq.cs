﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Job_Common.Req
{
    public class UpdateRequireDateReq
    {
        public int JobID { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime RequireDate { get; set; }
    }
}
