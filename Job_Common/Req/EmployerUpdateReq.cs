﻿namespace Job_Common.Req
{
    public class EmployerUpdateReq
    {
        public int EmployerID { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CompanyDescription { get; set; }
    }
}
