﻿namespace Job_Common.Req
{
    public class CreateAccountEmployeeHaveIDReq
    {
        public string AccountID { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string EmployeeName { get; set; }
        public string Phone { get; set; }
        public string CoverLetter { get; set; }
        public int EmployeeId { get; set; }
    }
}
