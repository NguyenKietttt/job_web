﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Job_Common.Req
{

    public class JobAddReq
    {
        public string JobName { get; set; }
        public List<string> JobDescription { get; set; }
        public List<string> JobRequire { get; set; }
        public string Salary { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime? RequireDate { get; set; }
        public string Experience { get; set; }
        public int EmployerID { get; set; }
        public string TitleName { get; set; }
        public string JobCategoryName { get; set; }
        public string JobTypeName { get; set; }
        public string CityName { get; set; }
    }
}
