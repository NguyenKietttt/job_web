﻿namespace Job_Common.Req
{
    public class EmployerReq
    {
        public string AccountID { get; set; }
        public int EmployerID { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string Image { get; set; }
        public string CompanyDescription { get; set; }
    }
}
