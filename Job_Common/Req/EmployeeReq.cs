﻿namespace Job_Common.Req
{
    public class EmployeeReq
    {
        public string AccountID { get; set; }
        public int EmployeeID { get; set; }
        public string Email { get; set; }
        public string EmployeeName { get; set; }
        public string Phone { get; set; }
        public string CoverLetter { get; set; }
    }
}
