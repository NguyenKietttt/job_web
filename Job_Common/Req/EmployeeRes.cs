﻿namespace Job_Common.Req
{
    public class EmployeeRes
    {
        public string EmployeeName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CoverLetter { get; set; }
        public string CV { get; set; }
        public string ApplyDate { get; set; }
    }
}
