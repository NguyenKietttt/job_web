﻿using Job_DAL;
using System.Collections.Generic;
using System.Linq;

namespace Job_BLL
{
    public sealed class GetData
    {
        #region Singleton
        private static GetData instance = null;

        public static GetData Instance
        {
            get
            {
                if (instance == null)
                    instance = new GetData();
                return instance;
            }
            private set { GetData.instance = value; }
        }
        private GetData() { }
        #endregion

        #region Dics
        public Dictionary<string, string> CitiesDic { get; private set; } = new Dictionary<string, string>();
        public Dictionary<int, string> CategoriesDic { get; private set; } = new Dictionary<int, string>();
        public Dictionary<int, string> TitleDic { get; private set; } = new Dictionary<int, string>();
        public Dictionary<int, string> JobTypeDic { get; private set; } = new Dictionary<int, string>();
        public Dictionary<int, string> UserTypeDic { get; private set; } = new Dictionary<int, string>();
        #endregion

        #region Methods
        public Dictionary<int, string> Check_UserTypeDic()
        {
            UserTypeDic = UserTypeRep.Get_ListUserType();

            return UserTypeDic;
        }

        public Dictionary<string, string> Check_CitiesDic()
        {
            CitiesDic = CityRep.Get_ListCities();

            return CitiesDic;
        }

        public Dictionary<int, string> Check_CategoriesDic()
        {
            CategoriesDic = JobCategoryRep.Get_ListJobCategories();

            return CategoriesDic;
        }

        public Dictionary<int, string> Check_TitleDic()
        {
            TitleDic = TitleRep.Get_ListTitle();

            return TitleDic;
        }

        public Dictionary<int, string> Check_JobTypeDic()
        {
            JobTypeDic = JobTypeRep.Get_ListJobType();

            return TitleDic;
        }

        public int Get_UserTypeID_FromDB(string value)
        {
            Check_UserTypeDic();

            int key = Instance.UserTypeDic.FirstOrDefault(x => x.Value == value).Key;

            return key;
        }

        public string Get_CityID_FromDB(string value)
        {
            Check_CitiesDic();

            string key = GetData.Instance.CitiesDic.FirstOrDefault(x => x.Value == value).Key;

            return key;
        }

        public int Get_JobCategoryID_FromDB(string value)
        {
            Check_CategoriesDic();

            int key = GetData.Instance.CategoriesDic.FirstOrDefault(x => x.Value == value).Key;

            return key;
        }

        public int Get_TitleID_FromDB(string value)
        {
            Check_TitleDic();

            int key = GetData.Instance.TitleDic.FirstOrDefault(x => x.Value == value).Key;

            return key;
        }

        public int Get_JobTypeID_FromDB(string value)
        {
            Check_JobTypeDic();

            int key = GetData.Instance.JobTypeDic.FirstOrDefault(x => x.Value == value).Key;

            return key;
        }
        #endregion
    }
}
