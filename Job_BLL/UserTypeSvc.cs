﻿using Job_Common.BLL;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Job_BLL
{
    public class UserTypeSvc : GenericSvc<UserTypeRep, UserType>
    {
        public SingleRsp Get_ListUserType()
        {
            var res = new SingleRsp();

            GetData.Instance.Check_UserTypeDic();

            List<string> title = GetData.Instance.UserTypeDic.Values.ToList();

            res.Data = title;

            return res;
        }
    }
}
