﻿using Job_Common.BLL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Job_BLL
{
    public class AccountSvc : GenericSvc<AccountRep, Account>
    {
        public SingleRsp Authenticate(AccountLoginReq account)
        {
            GetData.Instance.Check_UserTypeDic();

            var res = new SingleRsp();

            var listID = _rep.Authenticate(account).Count;

            if (listID < 1)
            {
                res.SetError("Your account does not exist!!!");

                return res;
            }

            string securityKey = "this is my custom Secret key for authnetication";

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                issuer: "smesk.in",
                audience: "readers",
                signingCredentials: signingCredentials,
                expires: DateTime.Now.AddHours(24)
                );

            string tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            string accountUser = null;
            int id = -1;

            int? checkUserType = _rep.Get_UserType_Account(account.AccountID);

            if (checkUserType == 0)
            {
                var employer = EmployerRep.Get_Employer_ByAccountID(account.AccountID);

                if (employer != null)
                {
                    accountUser = employer.AccountId;
                    id = employer.EmployerId;

                    var Employer = new
                    {
                        AccountName = accountUser,
                        EmployerID = id,
                        UserType = GetData.Instance.UserTypeDic.FirstOrDefault(p => p.Key == checkUserType).Value,
                        Token = tokenString
                    };

                    res.Data = Employer;

                    res.SetMessage("Login successful!!!");

                    return res;
                }
            }

            if (checkUserType == 1)
            {
                var employee = EmployeeRep.Get_Employee_ByAccountID(account.AccountID);

                if (employee != null)
                {
                    accountUser = employee.AccountId;
                    id = employee.EmployeeId;
                }
            }

            var Empployee = new
            {
                AccountName = accountUser,
                EmployeeID = id,
                UserType = GetData.Instance.UserTypeDic.FirstOrDefault(p => p.Key == checkUserType).Value,
                Token = tokenString
            };

            res.Data = Empployee;

            res.SetMessage("Login successful!!!");

            return res;
        }

        private bool CheckAccountID(string accountID)
        {
            List<string> listAccountID = _rep.Get_ListAccountID();

            bool check = listAccountID.Contains(accountID);

            return check;
        }

        public SingleRsp CreateAccountEmployer(CreateAccountEmployerNonIDReq account)
        {
            SingleRsp res = new SingleRsp();

            bool checkID = CheckAccountID(account.AccountID);

            if (checkID)
            {
                res.SetError("This account id is already being used by another account!!!");

                return res;
            }

            var regexMail = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

            if (!regexMail.IsMatch(account.Email))
            {
                res.SetError("Email is not correct!!!");

                return res;
            }

            CreateAccountEmployerHaveIDReq2 accountReq = new CreateAccountEmployerHaveIDReq2
            {
                AccountID = account.AccountID.Trim(),
                Password = account.Password.Trim(),
                Email = account.Email,
                EmployerId = EmployerSvc.Get_EmployerID_FromDB(),
                CompanyID = account.CompanyName,
                CompanyDescription = account.CompanyDescription,
                Address = account.Address,
                CityName = GetData.Instance.Get_CityID_FromDB(account.CityName)
            };

            res = _rep.CreateAccountEmployer(accountReq);
            res.SetMessage("Created successful!!!");

            return res;
        }

        public SingleRsp CreateAccountEmployee(CreateAccountEmployeeReq accountRes)
        {
            SingleRsp res = new SingleRsp();

            bool checkID = CheckAccountID(accountRes.AccountID);

            if (checkID)
            {
                res.SetError("This account id is already being used by another account!!!");

                return res;
            }

            var regexMail = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

            if (!regexMail.IsMatch(accountRes.Email))
            {
                res.SetError("Email is not correct!!!");

                return res;
            }

            var regexPhone = new Regex(@"(0[3|5|7|8|9]|01[2|6|8|9])+([0-9]{8})\b");

            if (!regexPhone.IsMatch(accountRes.Phone))
            {
                res.SetError("Phone is not correct!!!");

                return res;
            }

            CreateAccountEmployeeHaveIDReq accountReq = new CreateAccountEmployeeHaveIDReq
            {
                AccountID = accountRes.AccountID.Trim(),
                Password = accountRes.Password.Trim(),
                Email = accountRes.Email,
                EmployeeId = EmployeeSvc.Get_EmployeeID_FromDB(),
                EmployeeName = accountRes.EmployeeName,
                Phone = accountRes.Phone,
                CoverLetter = accountRes.CoverLetter
            };

            res = _rep.CreateAccountEmployee(accountReq);
            res.SetMessage("Created successful!!!");

            return res;
        }

    }
}
