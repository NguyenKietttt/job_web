﻿using Job_Common.BLL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Job_BLL
{
    public class EmployerSvc : GenericSvc<EmployerRep, Employer>
    {
        public SingleRsp Get_Employer_ByID(int EmployerID)
        {
            SingleRsp res = new SingleRsp();

            var employer = EmployerRep.Get_Employer_ByID(EmployerID);

            if (employer == null)
            {
                res.SetError("Employer does not exist!!!");

                return res;
            }

            List<EmployerReq> employerReqs = new List<EmployerReq>
            {
                employer
            };

            res.Data = employerReqs;

            return res;
        }

        public static int Get_EmployerID_FromDB()
        {
            int employerID;

            int employerCount = EmployerRep.Count_Employer();

            if (employerCount == 0)
            {
                employerID = 0;

                return employerID;
            }

            employerID = EmployerRep.GetMax_EmployerID() + 1;

            return employerID;
        }

        public async Task<SingleRsp> Upload_EmployerImgAsync(ImageReq imageReq)
        {
            SingleRsp res = new SingleRsp();

            string fullPath = null;

            Employer employer = EmployerRep.Get_Employer_ByAccountID(imageReq.AccountID);

            if (imageReq.File != null && employer != null)
            {
                double fileSize = (double)imageReq.File.Length / (double)(1024 * 1024);

                if (fileSize > 0 && fileSize < 1)
                {
                    var type = Path.GetExtension(imageReq.File.FileName);

                    if (type == ".png" || type == ".jpg")
                    {
                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=jobwebstore;AccountKey=bfAKdF3re+2NtXQDCLCPluP6RdMn6/k3yWpftmQ+gL2HCH8J2XxxIBQemllkNNA7neJZd2JIX/5Kd6SArCeGjA==;EndpointSuffix=core.windows.net");

                        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                        CloudBlobContainer container = blobClient.GetContainerReference("store");
                        if (await container.CreateIfNotExistsAsync())
                        {
                            await container.SetPermissionsAsync(new BlobContainerPermissions
                            {
                                PublicAccess = BlobContainerPublicAccessType.Blob
                            });
                        }

                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(imageReq.File.FileName);
                        blockBlob.Properties.ContentType = imageReq.File.ContentType;

                        using (var fileStream = imageReq.File.OpenReadStream())
                        {
                            await blockBlob.UploadFromStreamAsync(fileStream);
                        }

                        fullPath = blockBlob.Uri.AbsoluteUri;
                    }
                }
            }
            else
            {
                res.SetError("Please check file upload and AccountID!!!");

                return res;
            }

            employer.Image = fullPath;

            res = _rep.UpdateEmployerImg(employer);

            List<string> listPath = new List<string>
            {
                fullPath
            };

            res.Data = listPath;

            return res;
        }

        public SingleRsp Update_Employer(EmployerUpdateReq req)
        {
            SingleRsp res = new SingleRsp();

            EmployerReq checkEmployer = EmployerRep.Get_Employer_ByID(req.EmployerID);

            if (checkEmployer != null)
            {
                EmployeeUpdateNonIDReq updateReq2 = new EmployeeUpdateNonIDReq();

                if (req.CompanyName == null)
                    req.CompanyName = checkEmployer.CompanyName;

                if (req.Address == null)
                    req.Address = checkEmployer.Address;

                if (req.CompanyDescription == null)
                    req.CompanyDescription = checkEmployer.CompanyDescription;

                EmailReq email = new EmailReq();

                if (req.Email == null)
                    req.Email = checkEmployer.Email;
                else
                {
                    var regexMail = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

                    if (!regexMail.IsMatch(req.Email))
                    {
                        res.SetError("Email is not correct!!!");

                        return res;
                    }

                    email.Email = req.Email;
                }

                email.AccountID = checkEmployer.AccountID;

                AccountRep.Update_Email(email);

                res = _rep.Update_Employer(req);
                res.SetMessage("Update Successful!!!");

                return res;
            }

            res.SetError("Please check EmployerID!!!");

            return res;
        }
    }
}
