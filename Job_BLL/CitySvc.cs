﻿using Job_Common.BLL;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace Job_BLL
{
    public class CitySvc : GenericSvc<CityRep, City>
    {
        public SingleRsp Get_CityNames_FromDB()
        {
            SingleRsp res = new SingleRsp();

            GetData.Instance.Check_CitiesDic();

            List<string> listCityName = GetData.Instance.CitiesDic.Values.ToList();

            res.Data = listCityName;

            return res;
        }
    }
}
