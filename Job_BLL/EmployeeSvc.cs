﻿using Job_Common.BLL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Job_BLL
{
    public class EmployeeSvc : GenericSvc<EmployeeRep, Employee>
    {
        #region Static Methods
        public static int Get_EmployeeID_FromDB()
        {
            int employeeID;

            int employeeCount = EmployeeRep.Count_Employee();

            if (employeeCount == 0)
            {
                employeeID = 0;

                return employeeID;
            }

            employeeID = EmployeeRep.GetMax_EmployeeID() + 1;

            return employeeID;
        }
        #endregion

        public SingleRsp Get_ListEmployee_ByJobID(int JobID)
        {
            SingleRsp res = new SingleRsp();

            var temp = _rep.Get_ListEmployee_ByJobID(JobID);

            res.Data = temp;

            return res;
        }

        public SingleRsp Get_Info_Employee(int EmployeeID)
        {
            SingleRsp res = new SingleRsp();

            var employee = EmployeeRep.Get_Info_Employee(EmployeeID);

            if (employee == null)
            {
                res.SetError("Employee does not exist!!!");

                return res;
            }

            res.Data = employee;

            return res;
        }

        public SingleRsp Get_Image_ByAccountID(string AccountID)
        {
            SingleRsp res = new SingleRsp();

            var employee = _rep.Get_Image_ByAccountID(AccountID);

            if (employee == null)
            {
                res.SetError("Account does not exist!!!");

                return res;
            }

            res.Data = employee;

            return res;
        }

        public SingleRsp Update_Employee(EmployeeUpdateHaveIDReq req)
        {
            SingleRsp res = new SingleRsp();

            Employee checkEmployee = EmployeeRep.Get_Employee_ByID(req.EmployeeID);

            if (checkEmployee != null)
            {
                EmployeeUpdateNonIDReq updateReq2 = new EmployeeUpdateNonIDReq();

                if (req.EmployeeName == null)
                    updateReq2.EmployeeName = checkEmployee.EmployeeName;
                else
                    updateReq2.EmployeeName = req.EmployeeName;

                if (req.Phone == null)
                    updateReq2.Phone = checkEmployee.Phone;
                else
                {
                    var regexPhone = new Regex(@"(0[3|5|7|8|9]|01[2|6|8|9])+([0-9]{8})\b");

                    if (!regexPhone.IsMatch(req.Phone))
                    {
                        res.SetError("Phone is not correct!!!");

                        return res;
                    }

                    updateReq2.Phone = req.Phone;
                }

                if (req.CoverLetter == null)
                    updateReq2.CoverLetter = checkEmployee.CoverLetter;
                else
                    updateReq2.CoverLetter = req.CoverLetter;


                var account = EmployeeRep.Get_Account_ByEmployeeID(req.EmployeeID);

                EmailReq email = new EmailReq();

                if (req.Email == null)
                    email.Email = account.Email;
                else
                {
                    var regexMail = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

                    if (!regexMail.IsMatch(req.Email))
                    {
                        res.SetError("Email is not correct!!!");

                        return res;
                    }

                    email.Email = req.Email;
                }

                email.AccountID = checkEmployee.AccountId;

                AccountRep.Update_Email(email);

                res = _rep.Update_Employee(updateReq2);

                return res;
            }

            res.SetError("Please check EmployeeID!!!");

            return res;
        }
    }
}
