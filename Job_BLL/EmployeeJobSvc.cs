﻿using Job_Common.BLL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Job_BLL
{
    public class EmployeeJobSvc : GenericSvc<EmployeeJobRep, EmployeeJob>
    {
        public object Get_ListJob_Applied(int employeeID, int page)
        {
            List<JobListEmployeeAppliedReq> listJobs = _rep.Get_ListJob_Applied(employeeID);

            var offSet = (page - 1) * 10;
            var total = listJobs.Count();
            int totalPage = (total % 10) == 0 ? (int)(total / 10) : (int)((total / 10) + 1);
            var data = listJobs.OrderByDescending(p => p.PostDate).Skip(offSet).Take(10).ToList();

            var res = new
            {
                ListJob = data,
                totalRecord = total,
                totalPages = totalPage,
                Page = page,
                Size = 10
            };

            return res;
        }

        public async Task<SingleRsp> ApplyJobAsync(ApplyJobReq req)
        {
            SingleRsp res = new SingleRsp();

            bool check = _rep.Check_EmployeeJob_ByEmployerID_JobID(req.JobId, req.EmployeeId);

            Employee employee = EmployeeRep.Get_Employee_ByID(req.EmployeeId);

            if (employee == null)
            {
                res.SetError("Please check EmployeeID");

                return res;
            }

            int job = JobRep.Get_Job_ByID(req.JobId);

            if (job < 1)
            {
                res.SetError("Please check JobID");

                return res;
            }

            if (!check)
            {
                EmployeeJob employeeJob = new EmployeeJob()
                {
                    JobId = req.JobId,
                    EmployeeId = req.EmployeeId,
                    ApplyDate = DateTime.Now.Date
                };

                _rep.AddEmployeeJob(employeeJob);

                string fullPath = null;

                if (req.CV != null)
                {
                    double fileSize = (double)req.CV.Length / (1024 * 1024);

                    if (fileSize > 0 && fileSize < 1)
                    {
                        var type = Path.GetExtension(req.CV.FileName);

                        if (type == ".pdf")
                        {
                            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=jobwebstore;AccountKey=bfAKdF3re+2NtXQDCLCPluP6RdMn6/k3yWpftmQ+gL2HCH8J2XxxIBQemllkNNA7neJZd2JIX/5Kd6SArCeGjA==;EndpointSuffix=core.windows.net");

                            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                            CloudBlobContainer container = blobClient.GetContainerReference("cvstore");
                            if (await container.CreateIfNotExistsAsync())
                            {
                                await container.SetPermissionsAsync(new BlobContainerPermissions
                                {
                                    PublicAccess = BlobContainerPublicAccessType.Blob
                                });
                            }

                            CloudBlockBlob blockBlob = container.GetBlockBlobReference(req.CV.FileName);
                            blockBlob.Properties.ContentType = req.CV.ContentType;

                            using (var fileStream = req.CV.OpenReadStream())
                            {
                                await blockBlob.UploadFromStreamAsync(fileStream);
                            }

                            fullPath = blockBlob.Uri.AbsoluteUri;
                        }
                    }
                }
                else
                {
                    res.SetError("Please check file upload");

                    return res;
                }

                var account = EmployeeRep.Get_Account_ByEmployeeID(req.EmployeeId);

                Send_Email(req.JobId, employee.EmployeeName, account.Email, employee.Phone, employee.CoverLetter, fullPath);

                EmployeeRep.Update_EmployeeCV(req.EmployeeId, fullPath);
            }

            res.SetMessage("Apply Successful!!!");

            return res;
        }

        private void Send_Email(int jobID, string employeeName, string email, string phone, string coverLetter, string CV)
        {
            WebClient webClient = new WebClient();

            byte[] bytes = webClient.DownloadData(CV);

            var job = JobRep.Get_JobDetail(jobID);

            string to = job.Email;
            string subject = "Apply for job: " + job.JobName;
            string body =
                "Tên ứng viên: " + employeeName + "\n" +
                "Email: " + email + "\n" +
                "SĐT: " + phone + "\n" +
                "Thư giới thiệu: \n" + coverLetter;

            MemoryStream webPdf = new MemoryStream(bytes);

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("jobwebfindersystem@gmail.com", "Concaheo45456")
            };

            using (MailMessage mm = new MailMessage())
            {
                mm.From = new MailAddress("jobwebfindersystem@gmail.com", "Web JobFinder");
                mm.To.Add(to);
                mm.Subject = subject;
                mm.Body = body;
                mm.IsBodyHtml = false;

                Attachment attachment = new Attachment(webPdf, employeeName + "-CV", MediaTypeNames.Application.Pdf);
                mm.Attachments.Add(attachment);

                smtp.Send(mm);
            }
        }
    }
}
