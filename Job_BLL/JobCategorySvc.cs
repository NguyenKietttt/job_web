﻿using Job_Common.BLL;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace Job_BLL
{
    public class JobCategorySvc : GenericSvc<JobCategoryRep, JobCategory>
    {
        public SingleRsp Get_JobCategory_FromDB()
        {
            var res = new SingleRsp();

            GetData.Instance.Check_CategoriesDic();

            List<string> listCategory = GetData.Instance.CategoriesDic.Values.ToList();

            res.Data = listCategory;

            return res;
        }
    }
}
