﻿using Job_Common.BLL;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace Job_BLL
{
    public class TitleSvc : GenericSvc<TitleRep, Title>
    {
        public SingleRsp Get_ListTitle()
        {
            var res = new SingleRsp();

            GetData.Instance.Check_TitleDic();

            List<string> title = GetData.Instance.TitleDic.Values.ToList();

            res.Data = title;

            return res;
        }
    }
}
