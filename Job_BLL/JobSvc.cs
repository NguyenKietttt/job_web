﻿using Job_Common.BLL;
using Job_Common.Req;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Job_BLL
{
    public class JobSvc : GenericSvc<JobRep, Job>
    {
        #region Collections
        readonly Dictionary<string, int> listSalary = new Dictionary<string, int>()
            {
                { "từ 1,000,000 VND", 1},
                { "từ 3,000,000 VND", 3},
                { "từ 5,000,000 VND", 5},
                { "từ 10,000,000 VND", 10},
                { "từ 15,000,000 VND", 15},
                { "từ 20,000,000 VND", 20},
                { "từ 30,000,000 VND", 30}
            };

        readonly List<string> listSalaryString = new List<string>()
            {
                { "1 Tr - 3 Tr VND"},
                { "3 Tr - 5 Tr VND"},
                { "5 Tr - 10 Tr VND"},
                { "10 Tr - 15 Tr VND"},
                { "15 Tr - 20 Tr VND"},
                { "20 Tr - 30 Tr VND"}
            };
        #endregion

        #region Methods
        private int Get_JobID_FromDB()
        {
            int JobID;

            int jobCount = _rep.Count_Job();

            if (jobCount == 0)
            {
                JobID = 0;

                return JobID;
            }

            JobID = _rep.GetMax_JobID() + 1;

            return JobID;
        }

        public object Get_ListJob_Page(JobFilterReq req)
        {

            var temp = req.GetType().GetProperties();

            foreach (var item in temp)
            {
                var a = item.GetValue(req);

                if (a != null && a.ToString() == "string")
                {
                    item.SetValue(req, null);
                }
            }

            List<JobListReq> listJobs = new List<JobListReq>();
            int salaryInt = listSalary.FirstOrDefault(x => x.Key == req.Salary).Value;

            listJobs = _rep.Get_ListJob_ByKeyWord(req.KeyWord, req.City, req.JobType)
                .Where(
                p => p.Salary != null &&
                p.Salary != "" &&
                Convert.ToInt32(p.Salary.Split(' ')[0]) >= salaryInt
                ).ToList();

            if (listJobs.Count < 1)
            {
                listJobs = _rep.Get_ListJob_ByCompanyName(req.KeyWord, req.City, req.JobType)
                    .Where(
                    p => p.Salary != null &&
                    p.Salary != "" &&
                    Convert.ToInt32(p.Salary.Split(' ')[0]) >= salaryInt
                    ).ToList();
            }

            var offSet = (req.Page - 1) * 10;
            var total = listJobs.Count();
            int totalPage = (total % 10) == 0 ? (int)(total / 10) : (total / 10) + 1;
            var data = listJobs.OrderByDescending(p => p.PostDate).Skip(offSet).Take(10).ToList();

            var res = new
            {
                Data = data,
                totalRecord = total,
                totalPages = totalPage,
                req.Page,
                Size = 10
            };

            return res;
        }

        public object Get_ListJob_ByEmployerID(int employerID, int page)
        {
            EmployerReq employer = EmployerRep.Get_Employer_ByID(employerID);

            List<JobListEmployerReq> listJobs = _rep.Get_ListJob_ByEmployerID(employerID);

            EmployerJobReq employerJob = new EmployerJobReq
            {
                Employer = employer,
                ListJobs = listJobs
            };

            var offSet = (page - 1) * 10;
            var total = listJobs.Count();
            int totalPage = (total % 10) == 0 ? (int)(total / 10) : (int)((total / 10) + 1);
            var data = listJobs.OrderByDescending(p => p.PostDate).Skip(offSet).Take(10).ToList();

            var res = new
            {
                Employer = employer,
                ListJob = data,
                totalRecord = total,
                totalPages = totalPage,
                Page = page,
                Size = 10
            };

            return res;
        }

        public object Get_All_ListJob(int page)
        {
            List<JobListReq> listJobs = _rep.Get_All_ListJob();

            var offSet = (page - 1) * 10;
            var total = listJobs.Count();
            int totalPage = (total % 10) == 0 ? (int)(total / 10) : (int)((total / 10) + 1);
            var data = listJobs.OrderByDescending(p => p.PostDate).Skip(offSet).Take(10).ToList();

            var res = new
            {
                Data = data,
                totalRecord = total,
                totalPages = totalPage,
                Page = page,
                Size = 10
            };

            return res;
        }

        public SingleRsp Get_ListSalary_For_SearchJob()
        {
            SingleRsp res = new SingleRsp
            {
                Data = listSalary.Keys
            };

            return res;
        }

        public SingleRsp Get_ListSalary_For_AddJob()
        {
            SingleRsp res = new SingleRsp
            {
                Data = listSalaryString
            };

            return res;
        }

        public SingleRsp Get_JobDetail(int JobID)
        {
            SingleRsp res = new SingleRsp();

            JobDetailReq jobDetail = JobRep.Get_JobDetail(JobID);

            if (jobDetail == null)
            {
                res.SetError("Job does not exist!!!");

                return res;
            }

            var temp = new
            {
                jobDetail.JobName,
                JobDescription = jobDetail.JobDescription.Split('*').ToList(),
                JobRequire = jobDetail.JobRequire.Split('*').ToList(),
                jobDetail.Salary,
                PostDate = jobDetail.PostDate.Value.ToString("yyyy-MM-dd"),
                RequireDate = jobDetail.RequireDate.Value.ToString("yyyy-MM-dd"),
                jobDetail.Experience,
                jobDetail.EmployerId,
                jobDetail.CompanyName,
                jobDetail.Address,
                jobDetail.Email,
                jobDetail.Title,
                jobDetail.JobType,
                jobDetail.City,
                jobDetail.JobCategory,
                jobDetail.Image
            };

            res.Data = temp;

            return res;
        }

        public SingleRsp Add_Job(JobAddReq jobReq)
        {
            SingleRsp res = new SingleRsp();

            if (jobReq.JobName == "" || jobReq.JobName == null)
            {
                res.SetError("Job Name cannot be null or blank!!!");

                return res;
            }

            Job job = new Job
            {
                JobId = Get_JobID_FromDB(),
                JobName = jobReq.JobName
            };

            string fullDescription = "";

            if (jobReq.JobDescription != null)
            {
                foreach (var item in jobReq.JobDescription)
                {
                    var line = item.Trim() + "*";

                    fullDescription += line;
                }
            }

            if (fullDescription == "")
            {
                fullDescription = "Không có";
            }

            job.JobDescription = fullDescription;

            string fullRequire = "";

            if (jobReq.JobRequire != null)
            {
                foreach (var item in jobReq.JobRequire)
                {
                    var line = item.Trim() + "*";

                    fullRequire += line;
                }
            }

            if (fullRequire == "")
            {
                fullRequire = "Không có";
            }

            job.JobRequire = fullRequire;

            job.Salary = jobReq.Salary;
            job.PostDate = DateTime.Now.Date;
            job.RequireDate = jobReq.RequireDate;
            job.Experience = jobReq.Experience;

            job.EmployerId = jobReq.EmployerID;
            job.TitleId = GetData.Instance.Get_TitleID_FromDB(jobReq.TitleName);
            job.JobCategoryId = GetData.Instance.Get_JobCategoryID_FromDB(jobReq.JobCategoryName);
            job.JobTypeId = GetData.Instance.Get_JobTypeID_FromDB(jobReq.JobTypeName);
            job.CityId = GetData.Instance.Get_CityID_FromDB(jobReq.CityName);

            List<Job> listJob = new List<Job>
            {
                job
            };

            res = _rep.AddJobs(listJob);

            return res;
        }

        public SingleRsp Update_RequireDate(int jobID, DateTime requireDate)
        {
            SingleRsp res = new SingleRsp();

            int checkJOb = JobRep.Get_Job_ByID(jobID);

            if (checkJOb > 0)
            {
                res = _rep.Update_RequireDate(jobID, requireDate);

                return res;
            }

            res.SetError("Please check JobID and RequireDate!!!");

            return res;
        }

        public SingleRsp Delete_Job(int jobID)
        {
            SingleRsp res = new SingleRsp();

            var checkJob = JobRep.Get_Job_ByID(jobID);

            if (checkJob < 1)
            {
                res.SetError("Job does not exist !!!");

                return res;
            }

            var checkEmployee = EmployeeJobRep.Check_EmployeeJob_JobID(jobID);

            if (checkEmployee)
            {
                res.SetError("Employee had applied to this job, cannot delete this job!!!");

                return res;
            }

            res = _rep.Delete_Job(jobID);

            return res;
        }
        #endregion
    }
}
