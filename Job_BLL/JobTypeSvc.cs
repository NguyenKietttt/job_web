﻿using Job_Common.BLL;
using Job_Common.Rsp;
using Job_DAL;
using Job_DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace Job_BLL
{
    public class JobTypeSvc : GenericSvc<JobTypeRep, JobType>
    {
        public SingleRsp Get_ListJobType()
        {
            var res = new SingleRsp();

            GetData.Instance.Check_JobTypeDic();

            List<string> listJobType = GetData.Instance.JobTypeDic.Values.ToList();

            res.Data = listJobType;

            return res;
        }
    }
}
